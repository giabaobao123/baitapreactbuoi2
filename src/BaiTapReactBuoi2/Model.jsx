import React from 'react'

const Model = (props) => {
    let state = props.state
  return (
   <div className='text-center mt-5 Model'>
    <div className='img-model'>
    <img src="./glassesImage/model.jpg" className='imgModel' width={250} height={300} alt=""/>
    <img src={state.url} width={130} height={55} alt="" className='imgGlasses' />
    </div>
    <div className='content'>
    <p className='text-primary m-0'>{state.name} : {state.price}$</p>
    <p className='m-0'>{state.desc}</p>
    </div>
   </div>
  )
}

export default Model