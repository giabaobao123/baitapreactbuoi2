import React, { useState } from 'react'
import DanhSachSanPham from './DanhSachSanPham'
import '../Scss/index.scss'
import Model from './Model'
import data from '../DataJson/data.json'




const BaiTapReactBuoi2 = () => {
  let MangSanPHam = data
  let [state,setState] = useState(MangSanPHam[0])
  let setGlasses = (sp) => {
     setState(sp)
  }
  return (
    <div className='BaiTapReactBuoi2' style={{backgroundImage:'url("./glassesImage/background.jpg")'}}>
      <div className='overlay'></div>
        <header className='m-0 p-3 text-center'>
        <h3>TRY GLASSES APP ONLINE</h3>
        </header>
        <Model state = {state}/>
      <div className='container mt-3 DanhSachSanPham' style={{backgroundColor:'white'}}>
      <div className='row'>
       <DanhSachSanPham MangSanPham = {data} setGlasses = {setGlasses}/>
       </div>
      </div>
    </div>
  )
}

export default BaiTapReactBuoi2