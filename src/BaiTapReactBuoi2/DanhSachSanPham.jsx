import React from 'react'
const DanhSachSanPham = (props) => {
  let MangSanPham = props.MangSanPham
  let setGlasses = props.setGlasses
  return (
      MangSanPham.map((sp) => {
        return (
          <div className='col-2' key={sp.id}>
           <span  onClick={() => setGlasses(sp) }><img className='span' src={sp.url} width={100} height={50} alt="" /></span>
          </div>
        )
      })

  )
}

export default DanhSachSanPham